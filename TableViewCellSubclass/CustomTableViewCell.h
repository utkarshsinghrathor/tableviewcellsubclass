//
//  CustomTableViewCell.h
//  TableViewCellSubclass
//
//  Created by Mac-6 on 28/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *customView;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@end
