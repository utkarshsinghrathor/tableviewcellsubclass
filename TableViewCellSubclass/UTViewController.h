//
//  UTViewController.h
//  TableViewCellSubclass
//
//  Created by Mac-6 on 28/12/15.
//  Copyright (c) 2015 com.utkarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewView.h"

@interface UTViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblViewCustom;

@end
